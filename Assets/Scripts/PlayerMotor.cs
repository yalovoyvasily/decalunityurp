using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
public class PlayerMotor : MonoBehaviour
{
    [SerializeField]
    private Camera cam;
    [SerializeField]
    private Light flashlight;

    private Rigidbody rb;

    private Vector3 velocity, rotation, RotationCamera = Vector3.zero;


    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void Move (Vector3 _vel)
    {
        velocity = _vel;
    }

    public void Rotate(Vector3 _rotation)
    {
        rotation = _rotation;
    }

    public void RotateCam(Vector3 _RotationCamera)
    {
        RotationCamera = _RotationCamera;
    }

    void FixedUpdate()
    {
        PerformMove();
        PerformRotation();
    }

    void PerformMove()
    {
        if (velocity != Vector3.zero)
            rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);
    }

    void PerformRotation()
    {
        rb.MoveRotation(rb.rotation * Quaternion.Euler(rotation));
        if (cam != null)
            cam.transform.Rotate(-RotationCamera);
        flashlight.transform.Rotate(-RotationCamera);
    }
}
