using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // Start is called before the first frame update
    private Rigidbody rb;
    public float mouseSensitivity = 100.0f;
    public float clampAngle = 80.0f;

    private float rotY = 0.0f; // rotation around the up/y axis
    private float rotX = 0.0f; // rotation around the right/x axis

    public float speed = (float)0.1;
    public float jupmHeight = (float)10;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        var Force = ForceMode.Impulse;
        if (Input.GetKey(KeyCode.Space))
            //rb.MovePosition(new Vector3(0, 0, 0));
            rb.AddForce(0, jupmHeight, 0, Force);
        if (Input.GetKey(KeyCode.A))
            rb.MovePosition(new Vector3(rb.position.x - speed, rb.position.y, rb.position.z));
        if (Input.GetKey(KeyCode.D))
            rb.MovePosition(new Vector3(rb.position.x + speed, rb.position.y, rb.position.z));
        if (Input.GetKey(KeyCode.W))
            rb.MovePosition(new Vector3(rb.position.x , rb.position.y, rb.position.z + speed));
        if (Input.GetKey(KeyCode.S))
            rb.MovePosition(new Vector3(rb.position.x, rb.position.y, rb.position.z - speed));

        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = -Input.GetAxis("Mouse Y");

        rotY += mouseX * mouseSensitivity * Time.deltaTime;
        rotX += mouseY * mouseSensitivity * Time.deltaTime;

        rotX = Mathf.Clamp(rotX, -clampAngle, clampAngle);

        Quaternion localRotation = Quaternion.Euler(rotX, rotY, 0.0f);
        transform.rotation = localRotation;
    }
}
